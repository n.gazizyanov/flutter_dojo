import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_git_client_test/components/api.dart';
import 'package:flutter_git_client_test/components/constants.dart';
import 'package:flutter_git_client_test/components/models.dart';
import 'package:flutter_git_client_test/components/save.dart';
import 'package:flutter_git_client_test/components/widgets.dart';
import 'package:flutter_git_client_test/main.dart';
import 'package:flutter_git_client_test/pages/ExampleDetailView.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Example1 extends StatefulWidget{
  @override
  _Example1State createState() => _Example1State();
}

class _Example1State extends State<Example1> with AutomaticKeepAliveClientMixin<Example1> {
  List<int> items = [];
  int page = 1;

  RefreshController _refreshController = RefreshController(initialRefresh: true);

  void _onRefresh() {
    getMainRepoList().then((data){
      setState(() {items = [1, 2, 3, 4, 5, 6];});
      _refreshController.refreshCompleted();
    }).catchError((err){
      _refreshController.refreshCompleted();
    });
  }

  void _onLoading(){
    getMainRepoList().then((data){
      if(mounted){
        var next = items.last+1;
        setState(() {
          items.add(next);
        });
      }
      _refreshController.loadComplete();
    }).catchError((err){
      _refreshController.loadComplete();
    });

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: MaterialClassicHeader(),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          padding: EdgeInsets.all(5.0),
          itemBuilder: (c, i) => new RepoItemView(item: items[i]),
          itemExtent: 100.0,
          itemCount: items.length,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class RepoItemView extends StatelessWidget {
  const RepoItemView({
    Key key,
    @required this.item,
  }) : super(key: key);

  final int item;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: Slidable(
        actionExtentRatio: 0.2,
        actionPane: SlidableBehindActionPane(),
        secondaryActions: <Widget>[
          SlideAction(
            closeOnTap: true,
            child: SlideRightButton("EXMP", color: RRColors.BlueDark),
            onTap: (){
              log("success!");
            },
          ),
        ],
        child: Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 1.0, color: RRColors.Gray2),
            ),
            child: InkWell(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ExampleDetailView(parentItem:item)),
                  );
                },
                child: Center(
                    child:Text("Example")
                )
            )
        ),
      ),
    );
  }
}