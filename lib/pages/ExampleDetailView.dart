import 'dart:developer' show log;
import 'dart:math' show min;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_git_client_test/components/api.dart';
import 'package:flutter_git_client_test/components/constants.dart';
import 'package:flutter_git_client_test/components/models.dart';
import 'package:flutter_git_client_test/components/widgets.dart';
import 'package:flutter_git_client_test/main.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ExampleDetailView extends StatefulWidget {
  final int parentItem;
  ExampleDetailView({this.parentItem});

  @override
  _ExampleDetailViewState createState() => _ExampleDetailViewState();
}

class _ExampleDetailViewState extends State<ExampleDetailView> {
  int item;

  void getItem() {
    Future.delayed(Duration(milliseconds: 500)).then((b){
      setState(() {item = 1;});
    });
  }

  @override
  void initState() {
    getItem();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: item!=null
            ? ItemView(item: item, parentItem: widget.parentItem)
            : Center(child: ProgressBar()),
        appBar: AppBar(
          backgroundColor: RRColors.UglyColor,
          title: Text(widget.parentItem.toString()),
        ),
    );
  }
}

class ItemView extends StatelessWidget {
  final int parentItem;
  final int item;
  final List<int> commits;

  const ItemView({
    Key key,
    @required this.item, this.parentItem, this.commits,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        padding:  EdgeInsets.all(20.0),
        children: <Widget>[
          Container(
              height: 100,
              width: 100,
              child: Image.network("https://previews.123rf.com/images/chrisdorney/chrisdorney1306/chrisdorney130600109/20371194-example-rubber-stamp-over-a-white-background-.jpg")),

        ],
      ),
    );
  }
}