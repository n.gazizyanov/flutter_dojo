import 'dart:async';
import 'dart:convert' show jsonDecode;
import 'dart:developer';

import 'package:http/http.dart';
import 'package:http_extensions/http_extensions.dart';
import 'package:http_extensions_log/http_extensions_log.dart';
import 'package:logging/logging.dart';

import 'models.dart';

final http = ExtendedClient(
  inner: Client(),
  extensions: [
    LogExtension(
        logger: Logger("Http"),
        defaultOptions: LogOptions(
          logContent: true,
        )
    ),
  ],
);

void defaultErrorHandler(Object msg){
  log("error: " + msg.toString());
}

class ApiPaths {
  static const urlBase = 'https://api.github.com';
  static const repositories = "/repositories";
  static const google = "/users/google/repos";
}

Future<List<RepoMini>> getMainRepoList({int lastId}) {
  var completer = new Completer<List<RepoMini>>();
  var url = ApiPaths.urlBase + ApiPaths.repositories;
  if(lastId != null){
    url = url + "?since=$lastId";
  }
  http.get(url).catchError((error) {
    completer.completeError(error);
  }).then((Response response){
    if (response.statusCode == 200) {
      List bodyRes = response.body != null ? jsonDecode(response.body) : null;
      if (bodyRes != null) {
        var data = bodyRes.map((i) => RepoMini.fromJson(i)).toList();
        completer.complete(data);
      }
    } else {
      completer.completeError(response.body);
    }
  });
  completer.future.catchError(defaultErrorHandler);
  return completer.future;
}

Future<List<RepoMini>> getCustomRepoList({int page}) {
  var completer = new Completer<List<RepoMini>>();
  var url = ApiPaths.urlBase + ApiPaths.google;
  if(page != null){
    url = url + "?page=$page";
  }
  http.get(url).catchError((error) {
    completer.completeError(error);
  }).then((Response response){
    if (response.statusCode == 200) {
      List bodyRes = response.body != null ? jsonDecode(response.body) : null;
      if (bodyRes != null) {
        var data = bodyRes.map((i) => RepoMini.fromJson(i)).toList();
        completer.complete(data);
      }
    } else {
      completer.completeError(response.body);
    }
  });
  completer.future.catchError(defaultErrorHandler);
  return completer.future;
}
Future<RepoDetail> getRepoDetail(String url) {
  var completer = new Completer<RepoDetail>();
  // do yourself
  return completer.future;
}
Future getCommitsList(String url) {
  //do yourself
}