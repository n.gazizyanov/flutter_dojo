import 'package:flutter/material.dart';

class RRColors {
  RRColors._();
  static const Color Blue1 = Color(0xFF4B6D92);
  static const Color BlueDark = Color(0xFF213851);
  static const Color BlueBleak = Color(0xFFe9eff3);
  static const Color Gray1 = Color(0xFFA7A7A7);
  static const Color Gray2 = Color(0xFFcdcdcd);
  static const Color Accent = Color(0xFFD81B60);
  static const Color Red1 = Color(0xFFC02727);
  static const Color UglyColor = Color(0xFF6666ff);
}
