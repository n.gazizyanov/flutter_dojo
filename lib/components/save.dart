import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter_git_client_test/components/models.dart';
import 'package:shared_preferences/shared_preferences.dart';


List encondeToJson(List<RepoMini> list){
  List jsonList = List();
  list.map((item)=>
      jsonList.add(item.toJson())
  ).toList();
  return jsonList;
}
bool findById(List<RepoMini> list, int id){
  var data = list.where((item) => item.id == id);
  return data.length > 0;
}
void removeById(List<RepoMini> list, int id){
  list.removeWhere((item) => item.id == id);
}

Future<bool> saveRepoListToPref(List<RepoMini> repos) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var json = encondeToJson(repos);
  return prefs.setString('savedList', jsonEncode(json));
}

void saveRepoToPref(RepoMini repo) {
  getRepoListFromDisk().then((data){
    if(!findById(data, repo.id)){
      data.add(repo);
      saveRepoListToPref(data);
    }
  });
}
Future<bool> removeRepoFromPref(int id) {
  var completer = new Completer<bool>();
  getRepoListFromDisk().then((data){
    removeById(data, id);
    saveRepoListToPref(data).then((b){
      completer.complete(b);
    });
  });
  return completer.future;
}

Future<List<RepoMini>> getRepoListFromDisk() async {
  var completer = new Completer<List<RepoMini>>();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var json = prefs.getString('savedList');
  if (json != null) {
    List bodyRes = jsonDecode(json);
    var data = bodyRes.map((i) => RepoMini.fromJson(i)).toList();
    completer.complete(data);
  } else{
    completer.complete([]);
  }
  return completer.future;
}