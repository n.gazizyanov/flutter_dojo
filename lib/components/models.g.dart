// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RepoMini _$RepoMiniFromJson(Map<String, dynamic> json) {
  return RepoMini(
    json['id'] as int,
    json['name'] as String,
    json['description'] as String,
    json['url'] as String,
    json['owner'] == null
        ? null
        : OwnerMini.fromJson(json['owner'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RepoMiniToJson(RepoMini instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'url': instance.url,
      'owner': instance.owner,
    };

OwnerMini _$OwnerMiniFromJson(Map<String, dynamic> json) {
  return OwnerMini(
    json['id'] as int,
    json['login'] as String,
    json['avatar_url'] as String,
  );
}

Map<String, dynamic> _$OwnerMiniToJson(OwnerMini instance) => <String, dynamic>{
      'id': instance.id,
      'login': instance.login,
      'avatar_url': instance.avatar_url,
    };

RepoDetail _$RepoDetailFromJson(Map<String, dynamic> json) {
  return RepoDetail(
    json['name'] as String,
    json['description'] as String,
    json['language'] as String,
    json['forks_count'] as int,
    json['stargazers_count'] as int,
  );
}

Map<String, dynamic> _$RepoDetailToJson(RepoDetail instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'language': instance.language,
      'forks_count': instance.forks_count,
      'stargazers_count': instance.stargazers_count,
    };
