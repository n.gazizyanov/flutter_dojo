import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class RepoMini {
  int id;
  String name;
  String description;
  String url;
  OwnerMini owner;
  RepoMini(this.id, this.name, this.description, this.url, this.owner);

  factory RepoMini.fromJson(Map<String, dynamic> json) => _$RepoMiniFromJson(json);
  Map<String, dynamic> toJson() => _$RepoMiniToJson(this);
}

@JsonSerializable()
class OwnerMini {
  int id;
  String login;
  String avatar_url;
  OwnerMini(this.id, this.login, this.avatar_url);

  factory OwnerMini.fromJson(Map<String, dynamic> json) => _$OwnerMiniFromJson(json);
  Map<String, dynamic> toJson() => _$OwnerMiniToJson(this);
}

@JsonSerializable()
class RepoDetail {
  String name;
  String description;
  String language;
  int forks_count;
  int stargazers_count;
  RepoDetail(this.name, this.description, this.language, this.forks_count, this.stargazers_count);

  factory RepoDetail.fromJson(Map<String, dynamic> json) => _$RepoDetailFromJson(json);
  Map<String, dynamic> toJson() => _$RepoDetailToJson(this);
}