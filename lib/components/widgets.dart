import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'constants.dart';

class SlideRightButton extends StatelessWidget {
  final String text;
  final Color color;
  final bool enabled;

  const SlideRightButton(this.text, {@required this.color, this.enabled = true});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: (enabled) ? color : RRColors.Gray1),
        child: Center(
          child: Text(text,
              style: TextStyle(color: (enabled) ? Colors.white : RRColors.Gray1, fontSize: 13.0), softWrap: false),
        ));
  }
}

class ProgressBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(strokeWidth: 3.0);
  }
}

class DetailRow extends StatelessWidget {
  final String title;
  final String content;
  const DetailRow(this.title, this.content);


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("$title: "),
          Flexible(child: Text(content??"", softWrap: true)),
        ],
      ),
    );
  }
}

class ExampleList extends StatelessWidget {
  const ExampleList({
    Key key,
    @required this.itemList,
  }) : super(key: key);

  final List<int> itemList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(children: <Widget>[
          Text("Top"),
          Text("Bottom"),
        ]),
        Row(children: <Widget>[
          Text("Left"),
          Text("Right"),
        ]),
        Container(
          height: 100, width: 100,
          color: RRColors.BlueDark,
          child: Stack(children: <Widget>[
            Positioned(
                top: 1, left:1,
                child: Text("topLeft", style: TextStyle(color: Colors.white))
            ),
            Positioned(
                bottom: 1, right:1,
                child: Text("BottomRight", style: TextStyle(color: Colors.white))
            ),
          ]),
        ),
        Row(children: <Widget>[
          Expanded(child:Text("Left")),
          Text("Right"),
        ]),
        ...itemList.map((commit){
          return new ExampleListItem(commit);
        }).toList()],
    );
  }
}

class ExampleListItem extends StatelessWidget {
  final int commit;

  const ExampleListItem(this.commit, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(20))
      ),
      margin: EdgeInsets.all(5.0),
      padding: EdgeInsets.all(5.0),
      child: DetailRow("item list", commit.toString()),
    );
  }
}