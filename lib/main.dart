import 'package:flutter/material.dart';
import 'package:flutter_git_client_test/components/api.dart';
import 'package:flutter_git_client_test/components/constants.dart';
import 'package:flutter_git_client_test/components/widgets.dart';
import 'package:flutter_git_client_test/pages/ExampleDetailView.dart';
import 'package:flutter_git_client_test/pages/Example1.dart';
import 'package:logging/logging.dart';

void main() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    print('${record.loggerName} | ${record.level.name}: ${record.time}: ${record.message}');
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        backgroundColor: RRColors.UglyColor,
        accentColor: RRColors.Accent,
      ),
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: RRColors.UglyColor,
            title: TabBar(
              tabs: [
                Tab(text: "Example1"),
                Tab(text: "Example2"),
                Tab(text: "Example3"),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Example1(),
              ExampleList(itemList: [1, 2, 3, 4, 5, 6]),
              Icon(Icons.directions_bike)
            ],
          ),
        ),
      ),
    );
  }
}

